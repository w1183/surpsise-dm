const path = require( 'path' );
const nodeExternals = require('webpack-node-externals');

module.exports = {

    externalsPresets: { node: true }, // in order to ignore built-in modules like path, fs, etc.

    // entry files
    entry: './src/index.ts',

    // output bundles (location)
    output: {
        path: path.resolve( __dirname, 'dist' ),
        filename: 'index.js',
    },

    // file resolutions
    resolve: {
        extensions: [ '.ts', '.js' ],
    },

    externals: [nodeExternals()], // removes node_modules from your final bundle

    // loaders
    module: {
        rules: [
            {
                test: /\.tsx?/,
                use: 'ts-loader',
                exclude: /node_modules/,
            }
        ]
    }
};