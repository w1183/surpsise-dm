import { ElementHandle, Page } from "puppeteer-core";

export interface SocialNetwork {
    extractChromeCookies(): Promise<any>
    login(page: Page): Promise<void>
    extractFriends(page: Page): Promise<ElementHandle[]>
    convertToFriend(input: ElementHandle[]): Promise<Contact[]>
    messageContact(page: Page, contact: Contact, message: string): Promise<void>
    postPublicly(page: Page, text: string, image: string): Promise<string>
    getPublicStats(page: Page, postId: string): Promise<PublicPostStats>
}

export interface Contact {
    id: string;
    name: string;
    state: State;
}

export interface Template {
    id: string;
    text: string;
}

export enum State {
    NOT_STARTED,
    SENT_FIRST_MESSAGE,
    DELETED_FIRST_MESSAGE,
    SENT_SECOND_MESSAGE,
    DELETED_SECOND_MESSAGE,
    DONE
}

export interface PublicPostStats {
    postidReal: string;
    generated: boolean;
    dtime: number;
    likes: number;
    comments: number;
    views: number;
    repost: number;
}
