import { exit } from "process"
import { ElementHandle, Page } from "puppeteer-core"
import { Contact, PublicPostStats, SocialNetwork } from "./SocialNetwork"

// eslint-disable-next-line @typescript-eslint/no-var-requires
const chrome = require('./chrome')

const loginUrl = 'https://login.vk.com/'
const imListUrl = 'https://vk.com/im?act=create'
const imPersonUrl = 'https://vk.com/im?sel='
const imDeleteUrl = 'https://m.vk.com/mail?act=show&peer='
const feedUrl = 'https://vk.com/feed'
const publicPostsUrl = 'https://vk.com/wall{userId}?own=1'
const singlePostUrl = 'https://vk.com/'

export class VK implements SocialNetwork {
    scrollDelay: number
    maximumFriends: number

    constructor(scrollDelay = 2000,
        maximumFriends = 2000) {
        this.scrollDelay = scrollDelay
        this.maximumFriends = maximumFriends
    }

    async extractChromeCookies(): Promise<any> {
        return await chrome.getCookiesPromised(loginUrl, 'puppeteer', 'Default')
    }

    async extractFriends(page: Page): Promise<ElementHandle<Element>[]> {
        await page.goto(imListUrl)
        let items: Array<ElementHandle> = []

        try {
            let previousSize = -1
            let seenTimes = 0

            const elem = await page.$('._im_create_wrap');
            while (items.length < this.maximumFriends && seenTimes <= 3) {
                items = await page.$$('a._im_dialog')
                if (previousSize === items.length)
                    seenTimes++
                else {
                    seenTimes = 0
                    previousSize = items.length
                }

                await this.scrollDown(page, elem!)
                await page.waitForTimeout(this.scrollDelay)
            }
        } catch (e) {
            console.error(e)
        }
        return items
    }

    async convertToFriend(input: ElementHandle<Element>[]): Promise<Contact[]> {
        return await Promise.all(input.map(async (friend) => {
            return <Contact>{
                id: await friend.evaluate((el) => el.getAttribute('data-list-id')),
                name: (await friend.$eval('div', (el) => el.textContent))?.trim(),
            }
        }))
    }

    async messageContact(page: Page, contact: Contact, message: string): Promise<void> {
        const ready = await this.createDialog(page, contact.id)
        if (ready) {
            await this.typeText(page, message)
            await this.sendText(page)
            return this.removeLastMessage(page, contact)
        }
    }

    async login(page: Page): Promise<void> {
        // Cookies are setup for a different domain (login.vk.com) and thus must be first accessed
        await page.goto(`${loginUrl}?act=login`)
    }

    async postPublicly(page: Page, text?: string, image?: string): Promise<string> {
        await page.goto(feedUrl)
        await page.waitForSelector('#submit_post_box')
        const newPostInput = await page.$('#submit_post_box')
        await newPostInput!.click()

        if (text && image) {
            await this.addImage(page, image)
            await this.addText(page, text)
        } else if (image) {
            await this.addImage(page, image)
        } else if (text) {
            await this.addText(page, text)
        }
        await page.waitForTimeout(10000)
        await this.sendPost(page)

        await page.goto(feedUrl)
        await page.waitForSelector('#top_profile_link')
        const topProfile = await page.$('#top_profile_link')
        const profileHref = await page.evaluate((anchor) => { return anchor.getAttribute('href') }, topProfile) as string
        const userId = profileHref.split('/id')[1]

        await page.goto(publicPostsUrl.replace('{userId}', userId))
        const posts = await page.$$('a.post_link')
        const topPostLink = await page.evaluate((anchor) => { return anchor.getAttribute('href') }, posts[0]) as string
        return topPostLink.slice(1)
    }

    private async addText(page: Page, text: string) {
        await page.waitForSelector('#post_field')
        const inputField = await page.$('#post_field')
        await inputField!.click()

        return this.typeText(page, text)
    }

    private async addImage(page: Page, image: string) {
        const addPhotoSelector = await page.$('a.ms_item.ms_item_photo')
        await addPhotoSelector!.click()
        return this.attachFile(page, 'input.file', image)
    }

    async getPublicStats(page: Page, postId: string): Promise<PublicPostStats> {
        await page.goto(singlePostUrl + postId)
        const views = await this.fetchDataCountContent(page, 'span._views')
        const likes = await this.fetchTextContent(page, '.ReactionsPreview__count')
        const reposts = await this.fetchDataCountContent(page, 'div.share')
        const comments = await this.fetchDataCountContent(page, 'div.comment')

        const stats = {
            dtime: Date.now(),
            likes: likes,
            comments: comments,
            repost: reposts,
            views: views,
            generated: false,
            postidReal: postId,
        }
        return stats
    }

    private async fetchTextContent(page: Page, cssSelector: string) {
        const selector = await page.$(cssSelector)
        if (selector) {
            return Number(await selector?.evaluate(el => el.textContent))
        } else {
            return 0
        }
    }

    private async fetchDataCountContent(page: Page, cssSelector: string) {
        const selector = await page.$(cssSelector)
        if (selector) {
            return Number(await selector.evaluate(el => el.getAttribute("data-count")))
        } else {
            return 0
        }
    }

    private async removeLastMessage(page: Page, contact: Contact) {
        await page.goto(imDeleteUrl + contact.id)
        await page.waitForTimeout(1000)
        const messages = await page.$$('div.msg')
        const lastMessage = messages[messages.length - 1]
        await lastMessage.hover()
        await lastMessage.click()
        await page.waitForSelector('button.mailActs__icon_type_remove')
        const deleteButton = await page.$('button.mailActs__icon_type_remove')
        await deleteButton!.click()
        await page.waitForTimeout(500)
        await page.waitForSelector('div.mailDialog__confirmButton')
        const confirmButton = await page.$('div.mailDialog__confirmButton')
        await page.waitForTimeout(500)
        await confirmButton!.click()
    }

    private async scrollDown(page: Page, elem: ElementHandle): Promise<void> {
        if (elem !== null) {
            const boundingBox = await elem.boundingBox();
            await page.mouse.move(
                boundingBox!.x + boundingBox!.width / 2, // x
                boundingBox!.y + boundingBox!.height / 2 // y
            )
        } else {
            console.error("Can not scroll")
            exit(1)
        }

        await page.mouse.wheel({ deltaY: 3000 });
    }

    private async createDialog(page: Page, id: string): Promise<boolean> {
        await page.goto(`${imPersonUrl}${id}`)
        await page.waitForSelector(`#im_editable${id}`)
        const element = await page.$(`#im_editable${id}`)

        try {
            await element!.click();
            await page.waitForTimeout(3000);
            await page.focus(`#im_editable${id}`);
            console.log("ready to send")
            return true
        } catch (e) {
            console.log(`User ${id} might have been deleted`)
            return false
        }
    }

    private async typeText(page: Page, text: string) {
        await page.keyboard.type(text, { delay: 10 })
    }

    private async sendText(page: Page) {
        await page.keyboard.press('Enter')
        await page.keyboard.down('Control')
        await page.keyboard.press('Enter')
    }

    private async sendPost(page: Page) {
        await page.waitForSelector('#send_post')
        const submitButton = await page.$('#send_post')
        return submitButton!.click()
    }

    private async attachFile(page: Page, selector: string, filePath: string) {
        await page.waitForSelector(selector)
        const upload = await page.$(selector)
        return upload!.uploadFile(filePath)
    }
}
